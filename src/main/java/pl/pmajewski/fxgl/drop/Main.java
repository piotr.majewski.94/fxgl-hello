package pl.pmajewski.fxgl.drop;

import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;

public class Main extends GameApplication {

    @Override
    protected void initSettings(GameSettings gameSettings) {

    }

    public static void main(String[] args) {
        launch(args);
    }
}
